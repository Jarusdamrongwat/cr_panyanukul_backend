package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.Health;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class HealthRepository implements JpaRepository<Health, String> {

//    @Query(value = "SELECT p FROM Health p WHERE p.studentCode LIKE CONCAT('%',:studentCode,'%')")
//    List<Health> search_by_studentCode(@Param("studentCode") String studentCode);

    public List<Health> search_by_studentCode(String studentCode) {
        return null;
    }

    @Override
    public List<Health> findAll() {
        return null;
    }

    @Override
    public List<Health> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Health> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Health> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(Health health) {

    }

    @Override
    public void deleteAll(Iterable<? extends Health> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Health> S save(S s) {
        return null;
    }

    @Override
    public <S extends Health> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Health> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Health> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Health> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Health getOne(String s) {
        return null;
    }

    @Override
    public <S extends Health> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Health> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Health> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Health> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Health> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Health> boolean exists(Example<S> example) {
        return false;
    }
}
