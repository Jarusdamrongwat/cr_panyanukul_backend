package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.AbilityDetail;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AbilityDetailRepository implements JpaRepository<AbilityDetail, String> {

//    @Query(value = "SELECT p FROM AbilityDetail p WHERE p.abilityId.abilityId LIKE CONCAT('%',:abilityId,'%')")
//    List<AbilityDetail> search_by_abilityId(@Param("abilityId") String abilityId);

    public  List<AbilityDetail> search_by_abilityId(String abilityId) {
        return null;
    }

    @Override
    public List<AbilityDetail> findAll() {
        return null;
    }

    @Override
    public List<AbilityDetail> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<AbilityDetail> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<AbilityDetail> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(AbilityDetail abilityDetail) {

    }

    @Override
    public void deleteAll(Iterable<? extends AbilityDetail> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends AbilityDetail> S save(S s) {
        return null;
    }

    @Override
    public <S extends AbilityDetail> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<AbilityDetail> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends AbilityDetail> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<AbilityDetail> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public AbilityDetail getOne(String s) {
        return null;
    }

    @Override
    public <S extends AbilityDetail> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends AbilityDetail> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends AbilityDetail> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends AbilityDetail> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends AbilityDetail> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends AbilityDetail> boolean exists(Example<S> example) {
        return false;
    }
}
