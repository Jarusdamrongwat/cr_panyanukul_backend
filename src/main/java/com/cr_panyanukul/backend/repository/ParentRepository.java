package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.*;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ParentRepository implements JpaRepository<Parent, String> {

//    @Query(value = "SELECT p FROM Parent p WHERE p.studentCode LIKE CONCAT('%',:studentCode,'%')")
//    List<Parent> search_by_studentCode(@Param("studentCode") String studentCode);
//
//    @Query(value = "SELECT p FROM Parent p WHERE p.currentHomeId.currentHomeId LIKE CONCAT('%',:currentHomeId,'%')")
//    Parent search_by_currentAddress(@Param("currentHomeId") String currentHomeId);

    public List<Parent> search_by_studentCode(String studentCode) {
        return null;
    }

    public Parent search_by_currentAddress(String currentHomeId) {
        return null;
    }

    @Override
    public List<Parent> findAll() {
        return null;
    }

    @Override
    public List<Parent> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Parent> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Parent> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(Parent parent) {

    }

    @Override
    public void deleteAll(Iterable<? extends Parent> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Parent> S save(S s) {
        return null;
    }

    @Override
    public <S extends Parent> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Parent> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Parent> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Parent> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Parent getOne(String s) {
        return null;
    }

    @Override
    public <S extends Parent> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Parent> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Parent> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Parent> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Parent> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Parent> boolean exists(Example<S> example) {
        return false;
    }
}
