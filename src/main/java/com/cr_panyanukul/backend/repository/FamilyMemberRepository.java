package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.FamilyMember;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class FamilyMemberRepository implements JpaRepository<FamilyMember, String> {

//    @Query(value = "SELECT p FROM FamilyMember p WHERE p.studentCode.studentCode LIKE CONCAT('%',:studentCode,'%')")
//    List<FamilyMember> search_by_studentCode(@Param("studentCode") String studentCode);

    public List<FamilyMember> search_by_studentCode(String studentCode) {
        return null;
    }

    @Override
    public List<FamilyMember> findAll() {
        return null;
    }

    @Override
    public List<FamilyMember> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<FamilyMember> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<FamilyMember> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(FamilyMember familyMember) {

    }

    @Override
    public void deleteAll(Iterable<? extends FamilyMember> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends FamilyMember> S save(S s) {
        return null;
    }

    @Override
    public <S extends FamilyMember> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<FamilyMember> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends FamilyMember> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<FamilyMember> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public FamilyMember getOne(String s) {
        return null;
    }

    @Override
    public <S extends FamilyMember> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends FamilyMember> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends FamilyMember> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends FamilyMember> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends FamilyMember> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends FamilyMember> boolean exists(Example<S> example) {
        return false;
    }
}
