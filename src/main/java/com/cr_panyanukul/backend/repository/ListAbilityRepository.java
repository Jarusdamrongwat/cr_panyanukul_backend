package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.ListAbility;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ListAbilityRepository implements JpaRepository<ListAbility, String> {

//    @Query(value = "SELECT p FROM ListAbility p WHERE p.studentCode LIKE CONCAT('%',:studentCode,'%') AND p.year LIKE CONCAT('%',:year,'%')")
//    List<ListAbility> search_by_studentCode(@Param("studentCode") String studentCode, @Param("year") Long year);
//
//    @Query(value = "SELECT p FROM ListAbility p WHERE p.studentCode LIKE CONCAT('%',:studentCode,'%')")
//    List<ListAbility> search_by_studentCode(@Param("studentCode") String studentCode);
//
//    @Query(value = "SELECT p FROM ListAbility p WHERE p.abilityDetailId.abilityId.abilityHeaderId LIKE CONCAT('%',:abilityHeaderId,'%')")
//    List<ListAbility> search_by_header(@Param("abilityHeaderId") String abilityHeaderId);

    public List<ListAbility> search_by_studentCode(String studentCode, String year) {
        return null;
    }

    public List<ListAbility> search_by_studentCode(String studentCode) {
        return null;
    }

    public List<ListAbility> search_by_header(String abilityHeaderId) {
        return null;
    }

    @Override
    public List<ListAbility> findAll() {
        return null;
    }

    @Override
    public List<ListAbility> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<ListAbility> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<ListAbility> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(ListAbility listAbility) {

    }

    @Override
    public void deleteAll(Iterable<? extends ListAbility> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends ListAbility> S save(S s) {
        return null;
    }

    @Override
    public <S extends ListAbility> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<ListAbility> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends ListAbility> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<ListAbility> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public ListAbility getOne(String s) {
        return null;
    }

    @Override
    public <S extends ListAbility> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends ListAbility> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends ListAbility> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends ListAbility> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends ListAbility> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends ListAbility> boolean exists(Example<S> example) {
        return false;
    }
}
