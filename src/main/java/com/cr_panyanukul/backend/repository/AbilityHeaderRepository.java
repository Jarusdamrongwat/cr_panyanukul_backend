package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.AbilityHeader;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AbilityHeaderRepository implements JpaRepository<AbilityHeader, String> {

    @Override
    public List<AbilityHeader> findAll() {
        return null;
    }

    @Override
    public List<AbilityHeader> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<AbilityHeader> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<AbilityHeader> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(AbilityHeader abilityHeader) {

    }

    @Override
    public void deleteAll(Iterable<? extends AbilityHeader> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends AbilityHeader> S save(S s) {
        return null;
    }

    @Override
    public <S extends AbilityHeader> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<AbilityHeader> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends AbilityHeader> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<AbilityHeader> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public AbilityHeader getOne(String s) {
        return null;
    }

    @Override
    public <S extends AbilityHeader> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends AbilityHeader> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends AbilityHeader> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends AbilityHeader> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends AbilityHeader> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends AbilityHeader> boolean exists(Example<S> example) {
        return false;
    }
}
