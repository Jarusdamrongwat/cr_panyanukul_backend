package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.TransferStudent;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TransferStudentRepository implements JpaRepository<TransferStudent, String> {

//    @Query(value = "SELECT p FROM TransferStudent p WHERE p.studentCode LIKE CONCAT('%',:studentCode,'%') AND p.year LIKE CONCAT('%',:year,'%')")
//    public List<TransferStudent> search_by_studentCode_and_year(@Param("studentCode") String studentCode, @Param("year") Long year);
//
//    @Query(value = "SELECT p FROM TransferStudent p WHERE p.studentCode LIKE CONCAT('%',:studentCode,'%')")
//    public List<TransferStudent> search_by_studentCode(@Param("studentCode") String studentCode);

    public List<TransferStudent> search_by_studentCode_and_year(String studentCode, String year) {
        return null;
    }

    public List<TransferStudent> search_by_studentCode(String studentCode) {
        return null;
    }

    @Override
    public List<TransferStudent> findAll() {
        return null;
    }

    @Override
    public List<TransferStudent> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<TransferStudent> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<TransferStudent> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(TransferStudent transferStudent) {

    }

    @Override
    public void deleteAll(Iterable<? extends TransferStudent> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends TransferStudent> S save(S s) {
        return null;
    }

    @Override
    public <S extends TransferStudent> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<TransferStudent> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends TransferStudent> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<TransferStudent> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public TransferStudent getOne(String s) {
        return null;
    }

    @Override
    public <S extends TransferStudent> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends TransferStudent> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends TransferStudent> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends TransferStudent> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends TransferStudent> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends TransferStudent> boolean exists(Example<S> example) {
        return false;
    }
}
