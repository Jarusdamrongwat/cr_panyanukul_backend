package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.IepEvaluation;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class IepEvaluationRepository implements JpaRepository<IepEvaluation, String> {

//    @Query(value = "SELECT p FROM IepEvaluation p WHERE p.studentCode LIKE CONCAT('%',:studentCode,'%') AND p.year LIKE CONCAT('%',:year,'%')")
//    public List<IepEvaluation> search_by_studentCode_and_year(@Param("studentCode") String studentCode, @Param("year") Long year);
//
//    @Query(value = "SELECT p FROM IepEvaluation p WHERE p.studentCode LIKE CONCAT('%',:studentCode,'%')")
//    public List<IepEvaluation> search_by_studentCode(@Param("studentCode") String studentCode);

    public List<IepEvaluation> search_by_studentCode_and_year(String studentCode, @Param("year") String year) {
        return null;
    }

    public List<IepEvaluation> search_by_studentCode(String studentCode) {
        return null;
    }


    @Override
    public List<IepEvaluation> findAll() {
        return null;
    }

    @Override
    public List<IepEvaluation> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<IepEvaluation> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<IepEvaluation> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(IepEvaluation iepEvaluation) {

    }

    @Override
    public void deleteAll(Iterable<? extends IepEvaluation> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends IepEvaluation> S save(S s) {
        return null;
    }

    @Override
    public <S extends IepEvaluation> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<IepEvaluation> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends IepEvaluation> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<IepEvaluation> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public IepEvaluation getOne(String s) {
        return null;
    }

    @Override
    public <S extends IepEvaluation> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends IepEvaluation> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends IepEvaluation> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends IepEvaluation> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends IepEvaluation> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends IepEvaluation> boolean exists(Example<S> example) {
        return false;
    }
}
