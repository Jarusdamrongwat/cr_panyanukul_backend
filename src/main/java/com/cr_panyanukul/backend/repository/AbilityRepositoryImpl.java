package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.Ability;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class AbilityRepositoryImpl {
    public JdbcTemplate template;
    public String table = new Ability().TABLE;

    public List<Ability> search_by_abilityHeaderId(String abilityHeaderId) {
        String sql = "SELECT * FROM " + table + " WHERE ability_header_id LIKE CONCAT('%',?,'%')";
        return query(sql, new Object[] { abilityHeaderId });
    }

    public List<Ability> find_ability_group_by_subHeader() {
        String sql = "SELECT * FROM " + table + " GROUP BY ability_sub_header";
        return query(sql, new Object[] { });
    }

    private List<Ability> query(String sql, Object[] parameters) {
        List<Ability> ability = template.query(sql, parameters, new RowMapper<Ability>() {
            @Override
            public Ability mapRow(ResultSet resultSet, int i) throws SQLException {
                return null;
            }
        });
        return ability;
    }
}
