package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.Location;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class LocationRepository implements JpaRepository<Location, String> {

//    @Query(value = "SELECT s FROM Location s WHERE s.province LIKE CONCAT('%',:province,'%')")
//    public List<Location> search_by_province(@Param("province") String province);
//
//    @Query(value = "SELECT s FROM Location s WHERE s.district LIKE CONCAT('%',:district,'%')")
//    public List<Location> search_by_district(@Param("district") String district);

    public List<Location> search_by_province(String province) {
        return null;
    }

    public List<Location> search_by_district(String district) {
        return null;
    }

    @Override
    public List<Location> findAll() {
        return null;
    }

    @Override
    public List<Location> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Location> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Location> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(Location location) {

    }

    @Override
    public void deleteAll(Iterable<? extends Location> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Location> S save(S s) {
        return null;
    }

    @Override
    public <S extends Location> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Location> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Location> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Location> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Location getOne(String s) {
        return null;
    }

    @Override
    public <S extends Location> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Location> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Location> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Location> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Location> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Location> boolean exists(Example<S> example) {
        return false;
    }
}
