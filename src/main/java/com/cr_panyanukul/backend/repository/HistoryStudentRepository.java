package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.HistoryStudent;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class HistoryStudentRepository implements JpaRepository<HistoryStudent, String> {

//    @Query(value = "SELECT p FROM HistoryStudent p WHERE p.studentCode LIKE CONCAT('%',:studentCode,'%')")
//    List<HistoryStudent> search_by_studentCode(@Param("studentCode") String studentCode);
//
//    @Query(value = "SELECT p FROM HistoryStudent p WHERE p.year LIKE CONCAT('%',:year,'%')")
//    List<HistoryStudent> search_by_year(@Param("year") Long year);

    public List<HistoryStudent> search_by_studentCode(String studentCode) {
        return null;
    }

    public List<HistoryStudent> search_by_year(String year) {
        return null;
    }

    @Override
    public List<HistoryStudent> findAll() {
        return null;
    }

    @Override
    public List<HistoryStudent> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<HistoryStudent> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<HistoryStudent> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(HistoryStudent historyStudent) {

    }

    @Override
    public void deleteAll(Iterable<? extends HistoryStudent> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends HistoryStudent> S save(S s) {
        return null;
    }

    @Override
    public <S extends HistoryStudent> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<HistoryStudent> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends HistoryStudent> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<HistoryStudent> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public HistoryStudent getOne(String s) {
        return null;
    }

    @Override
    public <S extends HistoryStudent> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends HistoryStudent> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends HistoryStudent> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends HistoryStudent> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends HistoryStudent> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends HistoryStudent> boolean exists(Example<S> example) {
        return false;
    }
}
