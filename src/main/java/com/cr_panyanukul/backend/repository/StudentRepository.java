package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.Student;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class StudentRepository implements JpaRepository<Student, String> {

//    @Query(value = "SELECT s FROM Student s WHERE s.currentClassroom LIKE CONCAT('%',:class_name,'%')")
//    public List<Student> search_by_classroom(@Param("class_name") String class_name);
//
//    @Query(value = "SELECT s FROM Student s WHERE s.currentClassroom LIKE CONCAT('%',:class_name,'%') AND s.year LIKE CONCAT('%',:year,'%')")
//    public List<Student> search_by_classroom_and_year(@Param("class_name") String class_name, @Param("year") Integer year);
//
//    @Query(value = "SELECT s FROM Student s WHERE s.year LIKE CONCAT('%',:year,'%')")
//    public List<Student> search_by_year(@Param("year") Integer year);
//
//    @Query(value = "SELECT s FROM Student s WHERE s.crippleId LIKE CONCAT('%',:crippleId,'%')")
//    public List<Student> search_by_crippleId(@Param("crippleId") Integer crippleId);

    public List<Student> search_by_classroom(String class_name) {
        return null;
    }

    public List<Student> search_by_classroom_and_year(String class_name, Integer year) {
        return null;
    }

    public List<Student> search_by_year(Integer year) {
        return null;
    }

    public List<Student> search_by_crippleId(Integer crippleId) {
        return null;
    }

    @Override
    public List<Student> findAll() {
        return null;
    }

    @Override
    public List<Student> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Student> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Student> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(Student student) {

    }

    @Override
    public void deleteAll(Iterable<? extends Student> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Student> S save(S s) {
        return null;
    }

    @Override
    public <S extends Student> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Student> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Student> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Student> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Student getOne(String s) {
        return null;
    }

    @Override
    public <S extends Student> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Student> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Student> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Student> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Student> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Student> boolean exists(Example<S> example) {
        return false;
    }
}
