package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.Ability;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Repository
public class AbilityRepository {

    @Value("${jdbc.url}")
    private String url;

    public String table = new Ability().TABLE;
    public static String QUERY = "query";
    public static String SAVE = "save";

    public List<Ability> findAll() {
        String sql = "SELECT * FROM " + table;
        return query(sql, new Object[] { }, QUERY);
    }

    public Ability findById(String id) {
        String sql = "SELECT * FROM " + table + " WHERE ability_id = !0";
        return query(sql, new Object[] { id }, QUERY).get(0);
    }

    public Ability save(Ability entity) {
        String sql = "INSERT INTO " + table + "(";
        sql += "ability_id, ability_sub_header, ability_header_id";
        sql += ")";
        sql += "VALUES(?, ?, ?)";
        entity.setAbilityId(UUID.randomUUID().toString());
        return query(sql, new Object[] { entity.getAbilityId(), entity.getAbilitySubHeader(), entity.getAbilityHeaderId() }, SAVE).get(0);
    }

    public Boolean deleteById(String id) {
        String sql = "DELETE FROM " + table + " WHERE ability_id = ?";
        List<Ability> list = query(sql, new Object[] { id }, SAVE);
        Boolean deleteComplete = list.isEmpty() ? true: false;
        return deleteComplete;
    }

    public List<Ability> search_by_abilityHeaderId(String abilityHeaderId) {
        String sql = "SELECT * FROM " + table + " WHERE ability_header_id LIKE CONCAT('%',!0,'%')";
        return query(sql, new Object[] { abilityHeaderId }, QUERY);
    }

    public List<Ability> find_ability_group_by_subHeader() {
        String sql = "SELECT * FROM " + table + " GROUP BY ability_sub_header";
        return query(sql, new Object[] { }, QUERY);
    }

    private List<Ability> query(String sql, Object[] parameters, String type) {
        List<Ability> list = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(url);
            switch (type) {
                case "query":
                    list = this.typeQuery(connection, sql, parameters);
                    break;
                case "save":
                    list = this.typeSave(connection, sql, parameters);
                    break;
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return new ArrayList<>();
        }
    }

    private List<Ability> typeQuery(Connection connection, String sql, Object[] parameters) throws SQLException {
        List<Ability> list = new ArrayList<>();

        for (int i = 0; i < parameters.length; i++) {
            sql = sql.replaceAll("!" + i, "'" + parameters[i].toString() + "'");
        }

        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery(sql);

        while(result.next()) {
            Ability ability = new Ability();
            ability.setAbilityId(result.getString("ability_id"));
            ability.setAbilitySubHeader(result.getString("ability_sub_header"));
            ability.setAbilityHeaderId(result.getString("ability_header_id"));
            list.add(ability);
        }
        return list;
    }

    private List<Ability> typeSave(Connection connection, String sql, Object[] parameters) throws SQLException {
        List<Ability> list = new ArrayList<>();
        String id = parameters[0].toString();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        for (int i = 0; i < parameters.length; i++) {
            preparedStatement.setString((i+1), parameters[i].toString());
        }

        int row = preparedStatement.executeUpdate();

        if (row > 0) {
            sql = "SELECT * FROM " + table + " WHERE ability_id = '" + id + "'";
            list = this.typeQuery(connection, sql, parameters);
        }
        return list;
    }
}
