package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.Cripple;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CrippleRepository implements JpaRepository<Cripple, String> {

    @Override
    public List<Cripple> findAll() {
        return null;
    }

    @Override
    public List<Cripple> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Cripple> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Cripple> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(Cripple cripple) {

    }

    @Override
    public void deleteAll(Iterable<? extends Cripple> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Cripple> S save(S s) {
        return null;
    }

    @Override
    public <S extends Cripple> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Cripple> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Cripple> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Cripple> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Cripple getOne(String s) {
        return null;
    }

    @Override
    public <S extends Cripple> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Cripple> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Cripple> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Cripple> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Cripple> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Cripple> boolean exists(Example<S> example) {
        return false;
    }
}
