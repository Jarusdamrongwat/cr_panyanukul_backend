package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.CurrentAddress;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CurrentAddressRepository implements JpaRepository<CurrentAddress, String> {

//    @Query(value = "SELECT p FROM CurrentAddress p WHERE p.studentCode.studentCode LIKE CONCAT('%',:studentCode,'%')")
//    List<CurrentAddress> search_by_studentCode(@Param("studentCode") String studentCode);
//
//    @Query(value = "SELECT p FROM CurrentAddress p WHERE p.parentId.parentId LIKE CONCAT('%',:parentId,'%')")
//    List<CurrentAddress> search_by_ParentId(@Param("parentId") String parentId);

    public List<CurrentAddress> search_by_studentCode(String studentCode) {
        return null;
    }

    public List<CurrentAddress> search_by_ParentId(String parentId) {
        return null;
    }

    @Override
    public List<CurrentAddress> findAll() {
        return null;
    }

    @Override
    public List<CurrentAddress> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<CurrentAddress> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<CurrentAddress> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(CurrentAddress currentAddress) {

    }

    @Override
    public void deleteAll(Iterable<? extends CurrentAddress> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends CurrentAddress> S save(S s) {
        return null;
    }

    @Override
    public <S extends CurrentAddress> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<CurrentAddress> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends CurrentAddress> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<CurrentAddress> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public CurrentAddress getOne(String s) {
        return null;
    }

    @Override
    public <S extends CurrentAddress> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends CurrentAddress> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends CurrentAddress> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends CurrentAddress> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends CurrentAddress> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends CurrentAddress> boolean exists(Example<S> example) {
        return false;
    }
}
