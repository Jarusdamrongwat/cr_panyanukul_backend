package com.cr_panyanukul.backend.repository;

import com.cr_panyanukul.backend.model.AddressHomeParticular;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AddressHomeParticularRepository implements JpaRepository<AddressHomeParticular, String> {

//    @Query(value = "SELECT p FROM AddressHomeParticular p WHERE p.studentCode.studentCode LIKE CONCAT('%',:studentCode,'%')")
//    List<AddressHomeParticular> search_by_studentCode(@Param("studentCode") String studentCode);
//
//    @Query(value = "SELECT p FROM CurrentAddress p WHERE p.studentCode.year LIKE CONCAT('%',:year,'%')")
//    List<AddressHomeParticular> search_by_year(@Param("year") Integer year);

    public List<AddressHomeParticular> search_by_studentCode(String studentCode) {
        return null;
    }

    public List<AddressHomeParticular> search_by_year(Integer year) {
        return null;
    }

    @Override
    public List<AddressHomeParticular> findAll() {
        return null;
    }

    @Override
    public List<AddressHomeParticular> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<AddressHomeParticular> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<AddressHomeParticular> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(AddressHomeParticular addressHomeParticular) {

    }

    @Override
    public void deleteAll(Iterable<? extends AddressHomeParticular> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends AddressHomeParticular> S save(S s) {
        return null;
    }

    @Override
    public <S extends AddressHomeParticular> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<AddressHomeParticular> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends AddressHomeParticular> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<AddressHomeParticular> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public AddressHomeParticular getOne(String s) {
        return null;
    }

    @Override
    public <S extends AddressHomeParticular> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends AddressHomeParticular> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends AddressHomeParticular> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends AddressHomeParticular> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends AddressHomeParticular> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends AddressHomeParticular> boolean exists(Example<S> example) {
        return false;
    }
}
