package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.AbilityDetail;
import com.cr_panyanukul.backend.service.AbilityDetailServiceImpl;
import com.cr_panyanukul.backend.service.AbilityHeaderServiceImpl;
import com.cr_panyanukul.backend.service.AbilityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ability_detail")
public class AbilityDetailControllers {

    @Autowired
    private AbilityDetailServiceImpl service;

    @GetMapping("/list")
    public List<AbilityDetail> getAllAbility(){
        return service.getAllAbility();
    }

    @GetMapping("/list/{id}")
    public AbilityDetail getAbilityById(@PathVariable String id){
        return service.getOneAbility(id);
    }

    @PostMapping("/add/{abilityId}")
    public AbilityDetail createAbility(@RequestBody AbilityDetail ability, @PathVariable String abilityId) {
        ability.setAbilityId(abilityId);
        return service.createAbility(ability);
    }

    @GetMapping("/find_abilityId/{abilityId}")
    public List<AbilityDetail> abilityHeaderId(@PathVariable String abilityId){
        return service.getRepository().search_by_abilityId(abilityId);
    }

    @PutMapping("/update")
    public AbilityDetail updateAbility(@RequestBody AbilityDetail ability){
        return service.createAbility(ability);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteAbility(@PathVariable String id){
        service.deleteAbility(id);
    }
}
