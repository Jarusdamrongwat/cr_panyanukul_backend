package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.AddressHomeParticular;
import com.cr_panyanukul.backend.service.AddressHomeParticularServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/address_home_particular")
public class AddressHomeParticularControllers {

    @Autowired
    private AddressHomeParticularServiceImpl service;

    @GetMapping("/list")
    public List<AddressHomeParticular> getAllAddressHomeParticular(){
        return service.getAllAddressHomeParticular();
    }

    @GetMapping("/list/{id}")
    public AddressHomeParticular getAddressHomeParticularById(@PathVariable String id){
        return service.getOneAddressHomeParticular(id).get();
    }

    @GetMapping("/search_year/{year}")
    public List<AddressHomeParticular> search_year(@PathVariable Integer year){
        return service.getRepository().search_by_year(year);
    }

    @GetMapping("/find_studentCode/{studentCode}")
    public List<AddressHomeParticular> search_by_studentCode(@PathVariable String studentCode){
        return service.getRepository().search_by_studentCode(studentCode);
    }

    @PostMapping("/add")
    public AddressHomeParticular createAddressHomeParticular(@RequestBody AddressHomeParticular AddressHomeParticular){
        return service.createAddressHomeParticular(AddressHomeParticular);
    }

    @PutMapping("/update")
    public AddressHomeParticular updateAddressHomeParticular(@RequestBody AddressHomeParticular addressHomeParticular){
        return service.createAddressHomeParticular(addressHomeParticular);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteAddressHomeParticular(@PathVariable String id){
        service.deleteAddressHomeParticular(id);
    }
}
