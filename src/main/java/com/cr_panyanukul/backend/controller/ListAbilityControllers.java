package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.Ability;
import com.cr_panyanukul.backend.model.ListAbility;
import com.cr_panyanukul.backend.repository.AbilityRepository;
import com.cr_panyanukul.backend.service.AbilityDetailServiceImpl;
import com.cr_panyanukul.backend.service.AbilityServiceImpl;
import com.cr_panyanukul.backend.service.ListAbilityServiceImpl;
import com.cr_panyanukul.backend.service.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/list_ability")
public class ListAbilityControllers {

    @Autowired
    private ListAbilityServiceImpl service;

    @Autowired
    private AbilityDetailServiceImpl service_ability;

    @Autowired
    private StudentServiceImpl service_student;

    @GetMapping("/list")
    public List<ListAbility> getAllAbility(){
        return service.getAllAbility();
    }

    @GetMapping("/list/{id}")
    public ListAbility getAbilityById(@PathVariable String id){
        return service.getOneAbility(id).get();
    }

    @GetMapping("/find_studentCode/{studentCode}/{year}")
    public List<ListAbility> search_by_studentCode(@PathVariable String studentCode, @PathVariable String year){
        return service.getRepository().search_by_studentCode(studentCode,year);
    }

    @GetMapping("/find_studentCode/{studentCode}")
    public List<ListAbility> search_by_studentCode(@PathVariable String studentCode){
        return service.getRepository().search_by_studentCode(studentCode);
    }

    @GetMapping("/find_abilityHeaderId/{abilityHeaderId}")
    public List<ListAbility> abilityHeaderId(@PathVariable String abilityHeaderId){
        return service.getRepository().search_by_header(abilityHeaderId);
    }

    @PostMapping("/add/{abilityId}/{studentCode}")
    public ListAbility createAbility(@RequestBody ListAbility ability, @PathVariable String abilityId, @PathVariable String studentCode){
        ability.setAbilityDetailId(abilityId);
        ability.setStudentCode(studentCode);
        return service.createAbility(ability);
    }

    @PutMapping("/update")
    public ListAbility updateAbility(@RequestBody ListAbility ability){
        return service.createAbility(ability);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteAbility(@PathVariable String id){
        service.deleteAbility(id);
    }
}
