package com.cr_panyanukul.backend.controller;


import com.cr_panyanukul.backend.model.IepEvaluation;
import com.cr_panyanukul.backend.service.IepEvaluationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/iep_evaluation")
public class IepEvaluationControllers {

    @Autowired
    private IepEvaluationServiceImpl service;

    @GetMapping("/list")
    public List<IepEvaluation> getAllIepEvaluation(){
        return service.getAllIepEvaluation();
    }

    @GetMapping("/list/{id}")
    public IepEvaluation getIepEvaluationById(@PathVariable String id){
        return service.getOneIepEvaluation(id);
    }

    @GetMapping("/find_studentCode/{studentCode}")
    public List<IepEvaluation> search_by_studentCode(@PathVariable String studentCode){
        return service.getRepository().search_by_studentCode(studentCode);
    }

    @GetMapping("/find_studentCode/{studentCode}/{year}")
    public List<IepEvaluation> search_by_studentCode_and_year(@PathVariable String studentCode, @PathVariable String year){
        return service.getRepository().search_by_studentCode_and_year(studentCode, year);
    }

    @PostMapping("/add")
    public IepEvaluation createIepEvaluation(@RequestBody IepEvaluation IepEvaluation){
        return service.createIepEvaluation(IepEvaluation);
    }

    @PutMapping("/update")
    public IepEvaluation updateIepEvaluation(@RequestBody IepEvaluation IepEvaluation){
        return service.createIepEvaluation(IepEvaluation);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteIepEvaluation(@PathVariable String id){
        service.deleteIepEvaluation(id);
    }
}
