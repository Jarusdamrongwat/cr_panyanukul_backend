package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.Cripple;
import com.cr_panyanukul.backend.service.CrippleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cripple")
public class CrippleControllers {
    
    @Autowired
    private CrippleServiceImpl service;

    @GetMapping("/list")
    public List<Cripple> getAllCripple(){
        return service.getAllCripple();
    }

    @GetMapping("/list/{id}")
    public Cripple getCrippleById(@PathVariable String id){
        return service.getOneCripple(id).get();
    }

    @PostMapping("/add")
    public Cripple createCripple(@RequestBody Cripple Cripple){
        return service.createCripple(Cripple);
    }

    @PutMapping("/update")
    public Cripple updateCripple(@RequestBody Cripple Cripple){
        return service.createCripple(Cripple);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteCripple(@PathVariable String id){
        service.deleteCripple(id);
    }
}
