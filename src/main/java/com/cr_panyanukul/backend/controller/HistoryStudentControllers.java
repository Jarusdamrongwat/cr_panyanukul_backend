package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.Health;
import com.cr_panyanukul.backend.model.HistoryStudent;
import com.cr_panyanukul.backend.service.HealthServiceImpl;
import com.cr_panyanukul.backend.service.HistoryStudentServiceImpl;
import com.cr_panyanukul.backend.service.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/history_student")
public class HistoryStudentControllers {
    
    @Autowired
    private HistoryStudentServiceImpl service;

    @Autowired
    private StudentServiceImpl service_student;

    @GetMapping("/list")
    public List<HistoryStudent> getAllHistoryStudent(){
        return service.getAllHistoryStudent();
    }

    @GetMapping("/list/{id}")
    public HistoryStudent getHistoryStudentById(@PathVariable String id){
        return service.getOneHistoryStudent(id).get();
    }

    @GetMapping("/search_year/{year}")
    public List<HistoryStudent> search_year(@PathVariable String year){
        return service.getRepository().search_by_year(year);
    }

    @GetMapping("/find_studentCode/{studentCode}")
    public List<HistoryStudent> search_by_studentCode(@PathVariable String studentCode){
        return service.getRepository().search_by_studentCode(studentCode);
    }

    @PostMapping("/add/{studentCode}")
    public HistoryStudent createHistoryStudent(@RequestBody HistoryStudent historyStudent, @PathVariable String studentCode){
        historyStudent.setStudentCode(studentCode);
        return service.createHistoryStudent(historyStudent);
    }

    @PutMapping("/update")
    public HistoryStudent updateHistoryStudent(@RequestBody HistoryStudent HistoryStudent){
        return service.createHistoryStudent(HistoryStudent);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteHistoryStudent(@PathVariable String id){
        service.deleteHistoryStudent(id);
    }
}
