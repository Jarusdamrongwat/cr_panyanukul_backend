package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.FamilyMember;
import com.cr_panyanukul.backend.service.FamilyMemberServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/family_member")
public class FamilyMemberControllers {

    @Autowired
    private FamilyMemberServiceImpl service;

    @GetMapping("/list")
    public List<FamilyMember> getAllFamilyMember() {
        return service.getAllFamilyMember();
    }

    @GetMapping("/list/{id}")
    public FamilyMember getFamilyMemberById(@PathVariable String id) {
        return service.getOneFamilyMember(id).get();
    }

    @GetMapping("/find_studentCode/{studentCode}")
    public List<FamilyMember> search_by_studentCode(@PathVariable String studentCode){
        return service.getRepository().search_by_studentCode(studentCode);
    }

    @PostMapping("/add")
    public FamilyMember createFamilyMember(@RequestBody FamilyMember FamilyMember){
        return service.createFamilyMember(FamilyMember);
    }

    @PutMapping("/update")
    public FamilyMember updateFamilyMember(@RequestBody FamilyMember FamilyMember) {
        return service.createFamilyMember(FamilyMember);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteFamilyMember(@PathVariable String id) {
        service.deleteFamilyMember(id);
    }
}
