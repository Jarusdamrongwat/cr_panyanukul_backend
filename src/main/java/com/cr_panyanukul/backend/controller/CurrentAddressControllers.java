package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.CurrentAddress;
import com.cr_panyanukul.backend.service.CurrentAddressServiceImpl;
import com.cr_panyanukul.backend.service.ParentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/current_address")
public class CurrentAddressControllers {
    
    @Autowired
    private CurrentAddressServiceImpl service;

    @Autowired
    private ParentServiceImpl service_parent;

    @GetMapping("/list")
    public List<CurrentAddress> getAllCurrentAddress(){
        return service.getAllCurrentAddress();
    }

    @GetMapping("/list/{id}")
    public CurrentAddress getCurrentAddressById(@PathVariable String id){
        return service.getOneCurrentAddress(id);
    }

    @GetMapping("/find_studentCode/{studentCode}")
    public List<CurrentAddress> search_by_studentCode(@PathVariable String studentCode){
        return service.getRepository().search_by_studentCode(studentCode);
    }

    @GetMapping("/find_parent/{parentId}")
    public List<CurrentAddress> search_by_parentId(@PathVariable String parentId){
        return service.getRepository().search_by_ParentId(parentId);
    }

    @PostMapping("/add")
    public CurrentAddress createCurrentAddress(@RequestBody CurrentAddress currentAddress){
        return service.createCurrentAddress(currentAddress);
    }

    @PutMapping("/update")
    public CurrentAddress updateCurrentAddress(@RequestBody CurrentAddress CurrentAddress){
        return service.createCurrentAddress(CurrentAddress);
    }

    @PutMapping("/update/{parentId}")
    public CurrentAddress updateCurrentAddress(@RequestBody CurrentAddress currentAddress, @PathVariable String parentId){
        currentAddress.setParentId(parentId);
        return service.createCurrentAddress(currentAddress);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteCurrentAddress(@PathVariable String id){
        service.deleteCurrentAddress(id);
    }
}
