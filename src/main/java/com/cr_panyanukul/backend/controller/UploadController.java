package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.Student;
import com.cr_panyanukul.backend.service.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@RequestMapping("images")
public class UploadController {

    public static String uploadDirectory = null;

    @Autowired
    private StudentServiceImpl service;

    private void makeFolder(String folderName) {
        File directory = new File(folderName);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }

    private void initialDirectory() {
        if (uploadDirectory == null) {

            String path = "D:/cr_panyanukul";
            makeFolder(path);
            String directoryName = path.concat("/upload/");
            makeFolder(directoryName);

            uploadDirectory = directoryName;
        }
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<String> uploadImage(@RequestParam("image")MultipartFile file, @RequestParam("studentCode") String studentCode){
        StringBuilder file_name = new StringBuilder();
        Student student = service.getOneStudent(studentCode);
        String original_file_name = file.getOriginalFilename();

        initialDirectory();

        String modified_file_name = studentCode+"_"+file.getOriginalFilename();
        Path file_name_path = Paths.get(uploadDirectory, modified_file_name);
        file_name.append(modified_file_name);
        try {
            student.setImagePath(original_file_name);
            service.createStudent(student);
//            FileOutputStream outputStream = new FileOutputStream(uploadDirectory + file_name);
//            outputStream.write(file.getBytes());
            Files.write(file_name_path, file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<String>("Upload image Success Path = " + file_name_path,HttpStatus.OK);
    }

    @RequestMapping(value = "/displayImage", method = RequestMethod.GET)
    public void showImage(@RequestParam("studentCode") String studentCode, HttpServletResponse response) {
        Student student = service.getOneStudent(studentCode);

        initialDirectory();

        try {
            String file_name = student.getStudentCode() + "_" + student.getImagePath();
            InputStream classPath = new FileInputStream(uploadDirectory + file_name);
            BufferedImage image = ImageIO.read(classPath);

            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
            response.setContentType(MediaType.IMAGE_PNG_VALUE);
            ImageIO.write(image, "png", response.getOutputStream());
            ImageIO.write(image, "jpg", response.getOutputStream());
        }catch(IOException e){
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/displayAbsolutePath", method = RequestMethod.GET)
    public ResponseEntity<String> displayAbsolutePath() {
        initialDirectory();
        return new ResponseEntity<String>(uploadDirectory ,HttpStatus.OK);
    }

}
