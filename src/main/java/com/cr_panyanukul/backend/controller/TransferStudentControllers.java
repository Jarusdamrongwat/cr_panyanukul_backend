package com.cr_panyanukul.backend.controller;


import com.cr_panyanukul.backend.model.TransferStudent;
import com.cr_panyanukul.backend.service.TransferStudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transfer_student")
public class TransferStudentControllers {

    @Autowired
    private TransferStudentServiceImpl service;

    @GetMapping("/list")
    public List<TransferStudent> getAllTransferStudent(){
        return service.getAllTransferStudent();
    }

    @GetMapping("/list/{id}")
    public TransferStudent getTransferStudentById(@PathVariable String id){
        return service.getOneTransferStudent(id);
    }

    @GetMapping("/find_studentCode/{studentCode}")
    public List<TransferStudent> search_by_studentCode(@PathVariable String studentCode){
        return service.getRepository().search_by_studentCode(studentCode);
    }

    @GetMapping("/find_studentCode/{studentCode}/{year}")
    public List<TransferStudent> search_by_studentCode(@PathVariable String studentCode, @PathVariable String year){
        return service.getRepository().search_by_studentCode_and_year(studentCode, year);
    }

    @PostMapping("/add")
    public TransferStudent createTransferStudent(@RequestBody TransferStudent TransferStudent){
        return service.createTransferStudent(TransferStudent);
    }

    @PutMapping("/update")
    public TransferStudent updateTransferStudent(@RequestBody TransferStudent TransferStudent){
        return service.createTransferStudent(TransferStudent);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteTransferStudent(@PathVariable String id){
        service.deleteTransferStudent(id);
    }
}
