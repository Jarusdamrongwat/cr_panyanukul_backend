package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.Parent;
import com.cr_panyanukul.backend.repository.CurrentAddressRepository;
import com.cr_panyanukul.backend.repository.ParentRepository;
import com.cr_panyanukul.backend.service.CurrentAddressServiceImpl;
import com.cr_panyanukul.backend.service.ParentServiceImpl;
import com.cr_panyanukul.backend.service.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/parent")
public class ParentControllers {
    
    @Autowired
    private ParentServiceImpl service;

    @Autowired
    private CurrentAddressServiceImpl service_current;

    @Autowired
    private StudentServiceImpl service_student;

    @GetMapping("/list")
    public List<Parent> getAllParent() {
        return service.getAllParent();
    }

    @GetMapping("/list/{id}")
    public Parent getParentById(@PathVariable String id) {
        return service.getOneParent(id);
    }

    @GetMapping("/find_studentCode/{studentCode}")
    public List<Parent> search_by_studentCode(@PathVariable String studentCode){
        return service.getRepository().search_by_studentCode(studentCode);
    }

    @GetMapping("/find_current/{currentId}")
    public Parent search_by_currentAddress(@PathVariable String currentId){
        return service.getRepository().search_by_currentAddress(currentId);
    }

    @PostMapping("/add/{currentId}/{studentCode}")
    public Parent createParent(@RequestBody Parent parent, @PathVariable String currentId, @PathVariable String studentCode) {
        parent.setCurrentHomeId(currentId);
        parent.setStudentCode(studentCode);
        return service.createParent(parent);
    }

    @PutMapping("/update/{currentId}/{studentCode}")
    public Parent updateParent(@RequestBody Parent parent, @PathVariable String currentId, @PathVariable String studentCode) {
        parent.setCurrentHomeId(currentId);
        parent.setStudentCode(studentCode);
        return service.createParent(parent);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteParent(@PathVariable String id) {
        String listCurrentId = service.getOneParent(id).getCurrentHomeId();
        service.getRepository().deleteById(id);
        service_current.deleteCurrentAddress(listCurrentId);
    }
}
