package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.Health;
import com.cr_panyanukul.backend.service.HealthServiceImpl;
import com.cr_panyanukul.backend.service.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/health")
public class HealthControllers {
    
    @Autowired
    private HealthServiceImpl service;

    @GetMapping("/list")
    public List<Health> getAllHealth(){
        return service.getAllHealth();
    }

    @GetMapping("/list/{id}")
    public Health getHealthById(@PathVariable String id){
        return service.getOneHealth(id).get();
    }

    @GetMapping("/find_studentCode/{studentCode}")
    public List<Health> search_by_studentCode(@PathVariable String studentCode){
        return service.getRepository().search_by_studentCode(studentCode);
    }

    @PostMapping("/add/{studentCode}")
    public Health createHealth(@RequestBody Health health, @PathVariable String studentCode){
        health.setStudentCode(studentCode);
        return service.createHealth(health);
    }

    @PutMapping("/update")
    public Health updateHealth(@RequestBody Health Health){
        return service.createHealth(Health);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteHealth(@PathVariable String id){
        service.deleteHealth(id);
    }
}
