package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.Student;
import com.cr_panyanukul.backend.service.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentControllers {

    @Autowired
    private StudentServiceImpl service;

    @Autowired
    private AddressHomeParticularServiceImpl serviceAddress;

    @Autowired
    private HealthServiceImpl serviceHealth;

    @Autowired
    private HistoryStudentServiceImpl serviceHistory;

    @Autowired
    private ListAbilityServiceImpl serviceAbility;

    @Autowired
    private FamilyMemberServiceImpl memberService;

    @Autowired
    private CurrentAddressServiceImpl currentService;

    @Autowired
    private CrippleServiceImpl crippleService;

    @GetMapping("/list")
    public List<Student> getAllStudent() {
        return service.getAllStudent();
    }

    @GetMapping("/list/{id}")
    public Student getStudentById(@PathVariable String id) {
        return service.getOneStudent(id);
    }

    @GetMapping("/list_by_classname/{class_name}")
    public List<Student> search_by_classroom(@PathVariable String class_name){
        return service.getRepository().search_by_classroom(class_name);
    }

    @GetMapping("/list_by_classname_and_year/{class_name}/{year}")
    public List<Student> search_by_classroom(@PathVariable String class_name, @PathVariable Integer year){
        return service.getRepository().search_by_classroom_and_year(class_name,year);
    }

    @GetMapping("/search_year/{year}")
    public List<Student> search_year(@PathVariable Integer year){
        return service.getRepository().search_by_year(year);
    }

    @PostMapping(value = "/add/{addressId}/{currentId}/{crippleId}/{memberId}")
    public Student createStudent(@RequestBody Student student,
                                 @PathVariable String addressId,
                                 @PathVariable String currentId,
                                 @PathVariable String crippleId,
                                 @PathVariable String memberId) {
        student.setHomeParticularId(addressId);
        student.setCurrentHomeId(currentId);
        student.setCrippleId(crippleId);
        student.setMemberId(memberId);

        return service.createStudent(student);
    }

    @PutMapping("/update/{addressId}/{currentId}/{crippleId}/{memberId}")
    public Student updateStudent(@RequestBody Student updateStudent,
                                 @PathVariable String addressId,
                                 @PathVariable String currentId,
                                 @PathVariable String crippleId,
                                 @PathVariable String memberId) {
        updateStudent.setHomeParticularId(addressId);
        updateStudent.setCurrentHomeId(currentId);
        updateStudent.setCrippleId(crippleId);
        updateStudent.setMemberId(memberId);
        return service.createStudent(updateStudent);
    }

    @DeleteMapping("/delete/{abilityId}/{healthId}/{historyId}/{id}/{addressId}/{currentId}/{memberId}")
    public ResponseEntity<String> deleteStudent(@PathVariable String id,
                                                @PathVariable String abilityId,
                                                @PathVariable String healthId,
                                                @PathVariable String historyId,
                                                @PathVariable String addressId,
                                                @PathVariable String currentId,
                                                @PathVariable String memberId) {
        serviceAbility.deleteAbility(abilityId);
        serviceHealth.deleteHealth(healthId);
        serviceHistory.deleteHistoryStudent(historyId);
        service.deleteStudent(id);
        serviceAddress.deleteAddressHomeParticular(addressId);
        currentService.deleteCurrentAddress(currentId);
        memberService.deleteFamilyMember(memberId);
        return new ResponseEntity<String>("Delete Student Success", HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteStudent(@PathVariable String id) {
        service.deleteStudent(id);
        return new ResponseEntity<String>("Delete Student Success", HttpStatus.OK);
    }
}
