package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.AbilityHeader;
import com.cr_panyanukul.backend.service.AbilityHeaderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ability_header")
public class AbilityHeaderControllers {

    @Autowired
    private AbilityHeaderServiceImpl service;

    @GetMapping("/list")
    public List<AbilityHeader> getAllAbility(){
        return service.getAllAbility();
    }

    @GetMapping("/list/{id}")
    public AbilityHeader getAbilityById(@PathVariable String id){
        return service.getOneAbility(id);
    }

    @PostMapping("/add")
    public AbilityHeader createAbility(@RequestBody AbilityHeader ability){
        return service.createAbility(ability);
    }

    @PutMapping("/update")
    public AbilityHeader updateAbility(@RequestBody AbilityHeader ability){
        return service.createAbility(ability);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteAbility(@PathVariable String id){
        service.deleteAbility(id);
    }
}
