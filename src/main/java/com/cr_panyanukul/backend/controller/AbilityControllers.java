package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.Ability;
import com.cr_panyanukul.backend.service.AbilityHeaderServiceImpl;
import com.cr_panyanukul.backend.service.AbilityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ability")
public class AbilityControllers {

    @Autowired
    private AbilityServiceImpl service;

    @GetMapping("/list")
    public List<Ability> getAllAbility(){
        return service.getAllAbility();
    }

    @GetMapping("/list/{id}")
    public Ability getAbilityById(@PathVariable String id){
        return service.getOneAbility(id);
    }

    @GetMapping("/find_header/{abilityHeaderId}")
    public List<Ability> getAbilityByAbilityHeaderId(@PathVariable String abilityHeaderId){
        return service.getRepository().search_by_abilityHeaderId(abilityHeaderId);
    }

    @GetMapping("/find_ability_groupBy_subHeader")
    public List<Ability> findAbilityGroupBySubHeader(){
        return service.getRepository().find_ability_group_by_subHeader();
    }

    @PostMapping("/add/{abilityHeaderId}")
    public Ability createAbility(@RequestBody Ability ability,
                                 @PathVariable String abilityHeaderId) {
        ability.setAbilityHeaderId(abilityHeaderId);
        return service.createAbility(ability);
    }

    @PutMapping("/update")
    public Ability updateAbility(@RequestBody Ability ability){
        return service.createAbility(ability);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteAbility(@PathVariable String id){
        service.deleteAbility(id);
    }
}
