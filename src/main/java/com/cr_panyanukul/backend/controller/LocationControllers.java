package com.cr_panyanukul.backend.controller;

import com.cr_panyanukul.backend.model.Location;
import com.cr_panyanukul.backend.service.LocationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/location")
public class LocationControllers {

    @Autowired
    private LocationServiceImpl service;

    @GetMapping("/list")
    public List<Location> getAllLocation(){
        return service.getAllLocation();
    }

    @GetMapping("/list/{id}")
    public Location getLocationById(@PathVariable String id){
        return service.getOneLocation(id).get();
    }

    @GetMapping("/list/{province}")
    public List<Location> getLocationByProvince(@PathVariable String province){
        return service.getRepository().search_by_province(province);
    }

    @GetMapping("/list/{district}")
    public List<Location> getLocationByDistrict(@PathVariable String district){
        return service.getRepository().search_by_district(district);
    }

    @PostMapping("/add")
    public Location createLocation(@RequestBody Location Location){
        return service.createLocation(Location);
    }

    @PutMapping("/update")
    public Location updateLocation(@RequestBody Location Location){
        return service.createLocation(Location);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteLocation(@PathVariable String id){
        service.deleteLocation(id);
    }
}
