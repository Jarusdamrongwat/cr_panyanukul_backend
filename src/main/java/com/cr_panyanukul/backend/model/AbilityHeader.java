package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Table(name = "ability_header")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class AbilityHeader implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String abilityHeaderId;

    @Getter @Setter private String abilityHeader;
    @Getter @Setter private List<Ability> abilityId;
}
