package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "list_ability")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class ListAbility implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String listAbilityId;

    @Getter @Setter private Integer year;
    @Getter @Setter private String status;
    @Getter @Setter private String studentCode;
    @Getter @Setter private String abilityDetailId;
}
