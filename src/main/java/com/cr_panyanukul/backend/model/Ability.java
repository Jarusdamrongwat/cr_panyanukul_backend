package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.persistence.*;
import java.io.Serializable;

@Table(name = "ability")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Ability implements Serializable {
    private static final long serialVersionUID = 1L;

    public String TABLE = "ability";

    @Id
    @Getter @Setter private String abilityId;

    @Getter @Setter private String abilitySubHeader;
    @Getter @Setter private String abilityHeaderId;
}
