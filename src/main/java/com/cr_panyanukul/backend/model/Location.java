package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "location")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Location implements Serializable {

    @Id
    @Getter @Setter private String id;

    @Getter @Setter private String province;
    @Getter @Setter private String district;
    @Getter @Setter private String sub_district;
}
