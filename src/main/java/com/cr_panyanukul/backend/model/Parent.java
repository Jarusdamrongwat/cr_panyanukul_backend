package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "parent")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Parent implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String parentId;

    @Getter @Setter private String type;
    @Getter @Setter private String prefix;
    @Getter @Setter private String firstName;
    @Getter @Setter private String lastName;
    @Getter @Setter private Date birthDate;
    @Getter @Setter private String origin;
    @Getter @Setter private String nationality;
    @Getter @Setter private String religion;
    @Getter @Setter private String job;
    @Getter @Setter private String economicStatus;
    @Getter @Setter private String relationship;
    @Getter @Setter private boolean isGuardian;
    @Getter @Setter private Integer year;
    @Getter @Setter private String telephone;
    @Getter @Setter private String currentHomeId;
    @Getter @Setter private String studentCode;
}
