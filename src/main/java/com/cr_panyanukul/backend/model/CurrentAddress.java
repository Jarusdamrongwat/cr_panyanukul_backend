package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "current_address")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class CurrentAddress implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String currentHomeId;

    @Getter @Setter private String houseNumber;
    @Getter @Setter private String village;
    @Getter @Setter private String soi;
    @Getter @Setter private String road;
    @Getter @Setter private String district;
    @Getter @Setter private String subDistrict;
    @Getter @Setter private String province;
    @Getter @Setter private Integer postCode;
    @Getter @Setter private String parentId;
    @Getter @Setter private String studentCode;

}
