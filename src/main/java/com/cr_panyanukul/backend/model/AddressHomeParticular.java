package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "address_home_particular")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class AddressHomeParticular implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String homeParticularId;

    @Getter @Setter private String houseNumber;
    @Getter @Setter private String village;
    @Getter @Setter private String soi;
    @Getter @Setter private String road;
    @Getter @Setter private String district;
    @Getter @Setter private String subDistrict;
    @Getter @Setter private String province;
    @Getter @Setter private Integer postCode;
    @Getter @Setter private String studentCode;
}
