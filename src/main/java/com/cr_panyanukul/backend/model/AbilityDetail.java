package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Table(name = "ability_detail")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class AbilityDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String abilityDetailId;

    @Getter @Setter private String abilityDetail;
    @Getter @Setter private Integer abilityScore;
    @Getter @Setter private String abilityId;
    @Getter @Setter private List<ListAbility> listAbilityId;
}
