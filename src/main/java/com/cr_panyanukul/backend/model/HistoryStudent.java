package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "history_student")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class HistoryStudent implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String id;

    @Getter @Setter private String habit;
    @Getter @Setter private String classroom;
    @Getter @Setter private String teacherName;
    @Getter @Setter private String dormitoryName;
    @Getter @Setter private Integer year;
    @Getter @Setter private String studentCode;
}
