package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Table(name = "cripple")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Cripple implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String crippleId;

    @Getter @Setter private String crippleType;
    @Getter @Setter private List<Student> studentCode;
}
