package com.cr_panyanukul.backend.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "iep_evaluation")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class IepEvaluation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String id;

    @Getter @Setter private String semester;
    @Getter @Setter private Float percent;
    @Getter @Setter private Integer year;
    @Getter @Setter private String studentCode;

}
