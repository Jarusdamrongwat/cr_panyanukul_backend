package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Table(name = "student")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String studentCode;

    @Getter @Setter private String imagePath;
    @Getter @Setter private String prefix;
    @Getter @Setter private String firstName;
    @Getter @Setter private String lastName;
    @Getter @Setter private Date birthDate;
    @Getter @Setter private Date firstEnterSchool;
    @Getter @Setter private String idCard;
    @Getter @Setter private String origin;
    @Getter @Setter private String nationality;
    @Getter @Setter private String religion;
    @Getter @Setter private String previousSchool;
    @Getter @Setter private String congenitalDisease;
    @Getter @Setter private String personalMedicine;
    @Getter @Setter private String allergy;
    @Getter @Setter private String crippleCard;
    @Getter @Setter private Integer year;
    @Getter @Setter private String complexCripple;
    @Getter @Setter private String currentClassroom;
    @Getter @Setter private String startedClassroom;
    @Getter @Setter private String crippleId;
    @Getter @Setter private String memberId;
    @Getter @Setter private String currentHomeId;
    @Getter @Setter private String homeParticularId;
    @Getter @Setter private List<Parent> parentId;
    @Getter @Setter private List<HistoryStudent> id;
    @Getter @Setter private List<Health> healthId;
    @Getter @Setter private List<ListAbility> listAbilityId;

}
