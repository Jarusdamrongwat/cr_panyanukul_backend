package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "health")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Health implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String healthId;

    @Getter @Setter private Integer weight;
    @Getter @Setter private Integer height;
    @Getter @Setter private String historyHealth;
    @Getter @Setter private Integer year;
    @Getter @Setter private String studentCode;
}
