package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "transfer_student")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class TransferStudent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String id;

    @Getter @Setter private String title;
    @Getter @Setter private String school;
    @Getter @Setter private String classroom;
    @Getter @Setter private Date transferDate;
    @Getter @Setter private String reason;
    @Getter @Setter private Integer year;
    @Getter @Setter private String studentCode;

}
