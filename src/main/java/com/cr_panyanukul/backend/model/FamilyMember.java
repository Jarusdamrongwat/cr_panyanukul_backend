package com.cr_panyanukul.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.persistence.*;
import java.io.Serializable;

@Table(name = "family_member")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class FamilyMember implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Getter @Setter private String memberId;

    @Getter @Setter private Integer amountMembers;
    @Getter @Setter private String memberDetail;
    @Getter @Setter private String relationOfFamily;
    @Getter @Setter private String statusOfParent;
    @Getter @Setter private Integer amountSiblings;
    @Getter @Setter private String siblingsDetail;
    @Getter @Setter private String yourOrderInFamily;
    @Getter @Setter private String studentCode;
}
