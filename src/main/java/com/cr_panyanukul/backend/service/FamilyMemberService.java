package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.FamilyMember;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface FamilyMemberService {

    List<FamilyMember> getAllFamilyMember();
    Optional<FamilyMember> getOneFamilyMember(String id);
    FamilyMember createFamilyMember(FamilyMember FamilyMember);
    boolean deleteFamilyMember(String id);
    
}
