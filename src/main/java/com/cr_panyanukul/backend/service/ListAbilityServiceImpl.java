package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.ListAbility;
import com.cr_panyanukul.backend.repository.ListAbilityRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ListAbilityServiceImpl implements ListAbilityService{

    @Autowired @Getter @Setter
    private ListAbilityRepository repository;

    public List<ListAbility> getAllAbility(){
        return repository.findAll();
    }

    public Optional<ListAbility> getOneAbility(String id){
        return repository.findById(id);
    }

    public List<ListAbility> createAbility(List<ListAbility> ability){
        List<ListAbility> saveAbility = repository.saveAll(ability);
        return saveAbility;
    }

    public ListAbility createAbility(ListAbility ability){
        return repository.save(ability);
    }

    public boolean deleteAbility(String id){
        repository.deleteById(id);
        return true;
    }
}
