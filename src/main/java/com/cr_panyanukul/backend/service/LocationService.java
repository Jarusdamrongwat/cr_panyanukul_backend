package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Location;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface LocationService {

    public List<Location> getAllLocation();
    public Optional<Location> getOneLocation(String id);
    public Location createLocation(Location Student);
    public boolean deleteLocation(String id);
    
}
