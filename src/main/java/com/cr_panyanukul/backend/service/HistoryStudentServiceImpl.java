package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.HistoryStudent;
import com.cr_panyanukul.backend.repository.HistoryStudentRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HistoryStudentServiceImpl implements HistoryStudentService{
    
    @Autowired @Getter @Setter
    private HistoryStudentRepository repository;

    public List<HistoryStudent> getAllHistoryStudent(){
        return repository.findAll();
    }

    public Optional<HistoryStudent> getOneHistoryStudent(String id){
        return repository.findById(id);
    }

    public HistoryStudent createHistoryStudent(HistoryStudent Cripple){
        return repository.save(Cripple);
    }

    public boolean deleteHistoryStudent(String id){
        repository.deleteById(id);
        return true;
    }
}
