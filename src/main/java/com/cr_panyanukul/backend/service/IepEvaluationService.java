package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.IepEvaluation;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IepEvaluationService {

    public List<IepEvaluation> getAllIepEvaluation();
    public IepEvaluation getOneIepEvaluation(String id);
    public IepEvaluation createIepEvaluation(IepEvaluation IepEvaluation);
    public boolean deleteIepEvaluation(String id);
}
