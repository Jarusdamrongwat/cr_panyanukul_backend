package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.TransferStudent;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TransferStudentService {
    
    public List<TransferStudent> getAllTransferStudent();
    public TransferStudent getOneTransferStudent(String id);
    public TransferStudent createTransferStudent(TransferStudent TransferStudent);
    public boolean deleteTransferStudent(String id);
}
