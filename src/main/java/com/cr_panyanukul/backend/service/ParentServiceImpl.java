package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Parent;
import com.cr_panyanukul.backend.repository.ParentRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ParentServiceImpl implements ParentService{
    
    @Autowired @Getter @Setter
    private ParentRepository repository;

    public List<Parent> getAllParent(){
        return repository.findAll();
    }

    public Parent getOneParent(String id){
        return repository.findById(id).get();
    }

    public Parent createParent(Parent Parent){
        return repository.save(Parent);
    }

    public boolean deleteParent(String id){
        repository.deleteById(id);
        return true;
    }
}
