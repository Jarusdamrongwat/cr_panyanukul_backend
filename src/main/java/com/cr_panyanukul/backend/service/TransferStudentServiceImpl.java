package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.TransferStudent;
import com.cr_panyanukul.backend.repository.TransferStudentRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransferStudentServiceImpl implements TransferStudentService {

    @Autowired @Getter @Setter private TransferStudentRepository repository;

    public List<TransferStudent> getAllTransferStudent(){
        return repository.findAll();
    }

    public TransferStudent getOneTransferStudent(String id){
        return repository.findById(id).get();
    }

    public TransferStudent createTransferStudent(TransferStudent TransferStudent){
        return repository.save(TransferStudent);
    }

    public boolean deleteTransferStudent(String id){
        repository.deleteById(id);
        return true;
    }
}
