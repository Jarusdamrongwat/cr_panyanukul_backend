package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.ListAbility;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ListAbilityService {

    List<ListAbility> getAllAbility();
    Optional<ListAbility> getOneAbility(String id);
    List<ListAbility> createAbility(List<ListAbility> ability);
    ListAbility createAbility(ListAbility ability);
    boolean deleteAbility(String id);
}
