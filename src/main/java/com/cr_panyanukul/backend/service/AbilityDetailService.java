package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.AbilityDetail;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AbilityDetailService {

    List<AbilityDetail> getAllAbility();
    AbilityDetail getOneAbility(String id);
    AbilityDetail createAbility(AbilityDetail ability);
    boolean deleteAbility(String id);
}
