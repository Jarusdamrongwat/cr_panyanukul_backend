package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.HistoryStudent;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface HistoryStudentService {

    List<HistoryStudent> getAllHistoryStudent();
    Optional<HistoryStudent> getOneHistoryStudent(String id);
    HistoryStudent createHistoryStudent(HistoryStudent Cripple);
    boolean deleteHistoryStudent(String id);
    
}
