package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Ability;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface AbilityService {

    List<Ability> getAllAbility();
    Ability getOneAbility(String id);
    Ability createAbility(Ability ability);
    boolean deleteAbility(String id);
}
