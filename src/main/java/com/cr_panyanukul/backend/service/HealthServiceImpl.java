package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Health;
import com.cr_panyanukul.backend.repository.HealthRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HealthServiceImpl implements HealthService{
    
    @Autowired @Getter @Setter
    private HealthRepository repository;

    public List<Health> getAllHealth(){
        return repository.findAll();
    }

    public Optional<Health> getOneHealth(String id){
        return repository.findById(id);
    }

    public Health createHealth(Health Health){
        return repository.save(Health);
    }

    public boolean deleteHealth(String id){
        repository.deleteById(id);
        return true;
    }
}
