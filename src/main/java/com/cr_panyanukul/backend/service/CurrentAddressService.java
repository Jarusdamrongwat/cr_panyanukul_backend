package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.CurrentAddress;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface CurrentAddressService {

    List<CurrentAddress> getAllCurrentAddress();
    CurrentAddress getOneCurrentAddress(String id);
    CurrentAddress createCurrentAddress(CurrentAddress CurrentAddress);
    boolean deleteCurrentAddress(String id);
}
