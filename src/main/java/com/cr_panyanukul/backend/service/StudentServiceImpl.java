package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Student;
import com.cr_panyanukul.backend.repository.StudentRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService{
    
    @Autowired
    @Getter @Setter private StudentRepository repository;

    public List<Student> getAllStudent(){
        return repository.findAll();
    }

    public Student getOneStudent(String id){
        return repository.findById(id).get();
    }

    public Student createStudent(Student Student){
        return repository.save(Student);
    }

    public boolean deleteStudent(String id){
        repository.deleteById(id);
        return true;
    }

}
