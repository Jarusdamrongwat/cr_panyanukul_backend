package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Ability;
import com.cr_panyanukul.backend.model.AbilityHeader;
import com.cr_panyanukul.backend.repository.AbilityHeaderRepository;
import com.cr_panyanukul.backend.repository.AbilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AbilityHeaderServiceImpl implements AbilityHeaderService{

    @Autowired
    private AbilityHeaderRepository repository;

    public List<AbilityHeader> getAllAbility(){
        return repository.findAll();
    }

    public AbilityHeader getOneAbility(String id){
        return repository.findById(id).get();
    }

    public AbilityHeader createAbility(AbilityHeader ability){
        return repository.save(ability);
    }

    public boolean deleteAbility(String id){
        repository.deleteById(id);
        return true;
    }
}
