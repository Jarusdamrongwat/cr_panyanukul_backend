package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.CurrentAddress;
import com.cr_panyanukul.backend.repository.CurrentAddressRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CurrentAddressServiceImpl implements CurrentAddressService{
    
    @Autowired @Getter @Setter
    private CurrentAddressRepository repository;

    public List<CurrentAddress> getAllCurrentAddress(){
        return repository.findAll();
    }

    public CurrentAddress getOneCurrentAddress(String id){
        return repository.findById(id).get();
    }

    public CurrentAddress createCurrentAddress(CurrentAddress CurrentAddress){
        return repository.save(CurrentAddress);
    }

    public boolean deleteCurrentAddress(String id){
        repository.deleteById(id);
        return true;
    }
}
