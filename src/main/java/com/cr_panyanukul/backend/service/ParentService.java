package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Parent;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ParentService {

    List<Parent> getAllParent();
    Parent getOneParent(String id);
    Parent createParent(Parent Parent);
    boolean deleteParent(String id);
    
}
