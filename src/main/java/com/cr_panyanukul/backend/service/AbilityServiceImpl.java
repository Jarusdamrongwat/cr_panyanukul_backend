package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Ability;
import com.cr_panyanukul.backend.repository.AbilityRepository;
import com.cr_panyanukul.backend.repository.AbilityRepositoryImpl;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AbilityServiceImpl implements AbilityService{

    @Autowired @Getter @Setter
    private AbilityRepository repository;

    public List<Ability> getAllAbility(){
        return repository.findAll();
    }

    public Ability getOneAbility(String id){
        return repository.findById(id);
    }

    public Ability createAbility(Ability ability){
        return repository.save(ability);
    }

    public boolean deleteAbility(String id){
        repository.deleteById(id);
        return true;
    }
}
