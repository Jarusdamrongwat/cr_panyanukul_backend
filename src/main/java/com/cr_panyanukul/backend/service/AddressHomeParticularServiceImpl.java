package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.AddressHomeParticular;
import com.cr_panyanukul.backend.repository.AddressHomeParticularRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddressHomeParticularServiceImpl implements AddressHomeParticularService{

    @Autowired @Getter @Setter
    private AddressHomeParticularRepository repository;

    public List<AddressHomeParticular> getAllAddressHomeParticular(){
        return repository.findAll();
    }

    public Optional<AddressHomeParticular> getOneAddressHomeParticular(String id){
        return repository.findById(id);
    }

    public AddressHomeParticular createAddressHomeParticular(AddressHomeParticular addressHomeParticular){
        return repository.save(addressHomeParticular);
    }

    public boolean deleteAddressHomeParticular(String id){
        repository.deleteById(id);
        return true;
    }
}
