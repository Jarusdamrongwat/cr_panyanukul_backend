package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Ability;
import com.cr_panyanukul.backend.model.AbilityHeader;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AbilityHeaderService {

    public List<AbilityHeader> getAllAbility();
    public AbilityHeader getOneAbility(String id);
    public AbilityHeader createAbility(AbilityHeader ability);
    public boolean deleteAbility(String id);
}
