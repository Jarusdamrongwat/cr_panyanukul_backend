package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.AbilityDetail;
import com.cr_panyanukul.backend.repository.AbilityDetailRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AbilityDetailServiceImpl implements AbilityDetailService{

    @Autowired @Getter @Setter
    private AbilityDetailRepository repository;

    public List<AbilityDetail> getAllAbility(){
        return repository.findAll();
    }

    public AbilityDetail getOneAbility(String id){
        return repository.findById(id).get();
    }

    public AbilityDetail createAbility(AbilityDetail ability){
        return repository.save(ability);
    }

    public boolean deleteAbility(String id){
        repository.deleteById(id);
        return true;
    }
}
