package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.AddressHomeParticular;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface AddressHomeParticularService {

    List<AddressHomeParticular> getAllAddressHomeParticular();
    Optional<AddressHomeParticular> getOneAddressHomeParticular(String id);
    AddressHomeParticular createAddressHomeParticular(AddressHomeParticular AddressHomeParticular);
    boolean deleteAddressHomeParticular(String id);
    
}
