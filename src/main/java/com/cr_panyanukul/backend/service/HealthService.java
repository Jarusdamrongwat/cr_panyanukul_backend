package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Health;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface HealthService {

    List<Health> getAllHealth();
    Optional<Health> getOneHealth(String id);
    Health createHealth(Health Health);
    boolean deleteHealth(String id);
    
}
