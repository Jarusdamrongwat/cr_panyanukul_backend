package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Cripple;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface CrippleService {

    List<Cripple> getAllCripple();
    Optional<Cripple> getOneCripple(String id);
    Cripple createCripple(Cripple Cripple);
    boolean deleteCripple(String id);
    
}
