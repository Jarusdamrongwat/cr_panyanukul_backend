package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Student;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface StudentService {

    public List<Student> getAllStudent();
    public Student getOneStudent(String id);
    public Student createStudent(Student Student);
    public boolean deleteStudent(String id);
    
}
