package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.IepEvaluation;
import com.cr_panyanukul.backend.repository.IepEvaluationRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IepEvaluationServiceImpl implements IepEvaluationService{

    @Autowired
    @Getter @Setter private IepEvaluationRepository repository;

    public List<IepEvaluation> getAllIepEvaluation(){
        return repository.findAll();
    }

    public IepEvaluation getOneIepEvaluation(String id){
        return repository.findById(id).get();
    }

    public IepEvaluation createIepEvaluation(IepEvaluation IepEvaluation){
        return repository.save(IepEvaluation);
    }

    public boolean deleteIepEvaluation(String id){
        repository.deleteById(id);
        return true;
    }
}
