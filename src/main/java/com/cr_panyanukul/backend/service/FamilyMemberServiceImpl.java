package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.FamilyMember;
import com.cr_panyanukul.backend.repository.FamilyMemberRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FamilyMemberServiceImpl implements FamilyMemberService{
    
    @Autowired @Getter @Setter
    private FamilyMemberRepository repository;

    public List<FamilyMember> getAllFamilyMember(){
        return repository.findAll();
    }

    public Optional<FamilyMember> getOneFamilyMember(String id){
        return repository.findById(id);
    }

    public FamilyMember createFamilyMember(FamilyMember FamilyMember){
        return repository.save(FamilyMember);
    }

    public boolean deleteFamilyMember(String id){
        repository.deleteById(id);
        return true;
    }
}
