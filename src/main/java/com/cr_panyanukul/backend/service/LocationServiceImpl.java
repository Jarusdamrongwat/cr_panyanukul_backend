package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Location;
import com.cr_panyanukul.backend.repository.LocationRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class LocationServiceImpl implements LocationService{
    
    @Autowired
    @Getter @Setter private LocationRepository repository;

    public List<Location> getAllLocation(){
        return repository.findAll();
    }

    public Optional<Location> getOneLocation(String id){
        return repository.findById(id);
    }

    public Location createLocation(Location Location){
        return repository.save(Location);
    }

    public boolean deleteLocation(String id){
        repository.deleteById(id);
        return true;
    }

}
