package com.cr_panyanukul.backend.service;

import com.cr_panyanukul.backend.model.Cripple;
import com.cr_panyanukul.backend.repository.CrippleRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CrippleServiceImpl implements CrippleService{
    
    @Autowired @Getter @Setter
    private CrippleRepository repository;

    public List<Cripple> getAllCripple(){
        return repository.findAll();
    }

    public Optional<Cripple> getOneCripple(String id){
        return repository.findById(id);
    }

    public Cripple createCripple(Cripple Cripple){
        return repository.save(Cripple);
    }

    public boolean deleteCripple(String id){
        repository.deleteById(id);
        return true;
    }
}
