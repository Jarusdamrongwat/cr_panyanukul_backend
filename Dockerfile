FROM openjdk:8-jdk-alpine

ADD target/CRPBackend.jar CRPBackend.jar

EXPOSE 8081

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /CRPBackend.jar" ]
